const express = require('express');
const hbs = require('hbs');

const path = require('path');

const app = express();
const port = process.env.PORT || 3030
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)
app.use(express.static(publicDirectoryPath))

app.get('', (req, res) => {
  res.render('index', {
    year: (new Date()).getFullYear(),
    title: 'HOME'
  })
})

app.get('/about', (req, res) => {
  res.render('about', {
    year: (new Date()).getFullYear(),
    title: 'ABOUT'
  })
})

app.get('/contact', (req, res) => {
  res.render('contact', {
    year: (new Date()).getFullYear(),
    title: 'CONTACT'
  })
})

app.get('/faqs', (req, res) => {
  res.render('faqs', {
    year: (new Date()).getFullYear(),
    title: 'F.A.Qs'
  })
})

app.get('/portfolio', (req, res) => {
  res.render('portfolio', {
    year: (new Date()).getFullYear(),
    title: 'PORTFOLIO'
  })
})

app.get('/services', (req, res) => {
  res.render('services', {
    year: (new Date()).getFullYear(),
    title: 'SERVICES'
  })
})




app.get('*', (req, res) => {
  res.render('404')
})

app.listen(port, () => {
  console.log('listening on port: ', port);
})